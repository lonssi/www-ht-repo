// Add a new reservation
var addReservation = function(data) {
	var user = Meteor.user();
	if (user && user._id && data.time && data.table) {
		Reservations.insert({"userId": user._id, "userName": user.profile.name, "table": data.table, "time": data.time.toDate()});
	}
}

// Remove a reservation
var removeReservation = function(id) {
	Reservations.remove({"_id": id});
}

// Find coming reservations (sorted by time)
var findReservations = function(findObj) {
	findObj = findObj || {};
	return Reservations.find(_.extend({"time": {"$gte": new Date()}}, findObj), {
        "sort": {"time": 1}
    }).fetch();
}

// Find reservations that have already passed (sorted by time)
var findOldReservations = function(findObj) {
	findObj = findObj || {};
	return Reservations.find(_.extend({"time": {"$lte": new Date()}}, findObj), {
        "sort": {"time": 1}
    }).fetch();
}

// Find a reservation according to a specific time
var findReservationByTime = function(data) {
	return Reservations.findOne({"time": data.time && data.time.toDate(), "table": data.table});
}

Dispatcher.register(function(payload) {
	switch(payload.actionType) {
		case "REMOVE_RESERVATION_BUTTON_CLICKED":
			removeReservation(payload.data);
			break;
		case "ADD_RESERVATION_BUTTON_CLICKED":
			addReservation(payload.data);
			break;
	}
});

ReservationStore = {

	getReservationsCount: function() {
		return Reservations.find().count();
	},

	getReservations: function() {
		return Reservations.find();
	},

	findReservationByTime: findReservationByTime,
	findReservations: findReservations,
	findOldReservations: findOldReservations

}
