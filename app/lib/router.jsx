var title = "Reservator";
DocHead.setTitle(title);

// Default routing destination
FlowRouter.route('/', {
	name: 'main',
	triggersEnter: [function(context, redirect) {
	}],

	action: function(params, queryParams) {

		var metaInfo = {name: "Description", content: "Welcome page for Reservation service"};
		DocHead.addMeta(metaInfo);

		ReactLayout.render(MainLayout, {
			content:
				<div>
					<Welcome/>
				</div>
		});

	}
});

FlowRouter.route('/reservations', {
	name: 'reservations',
	triggersEnter: [function(context, redirect) {
	}],

	action: function(params, queryParams) {

		var metaInfo = {name: "Description", content: "In this page the user can examine and remove his/her reservations"};
		DocHead.addMeta(metaInfo);

		// Show user reservations if the user is logged in
		var loggedIn = Meteor.loggingIn() || Meteor.userId();
		var content = loggedIn ? <div><ReservationList/><ReservationHistoryList/></div>
	 		: <p>Please log in to access your reservations!</p>

		ReactLayout.render(MainLayout, {
			content: content
		});
	}
});

FlowRouter.route('/laundry', {
	name: 'laundry',
	triggersEnter: [function(context, redirect) {
	}],

	action: function(params, queryParams) {

		var metaInfo = {name: "Description", content: "Reservation list page for laundry"};
		DocHead.addMeta(metaInfo);

		ReactLayout.render(MainLayout, {
			content:
				<div>
					<h2 className="sub-header">Laundry</h2>
					<br/>
					<WeekList table={"laundry"}/>
				</div>
		});
	}
});

FlowRouter.route('/sauna', {
	name: 'sauna',
	triggersEnter: [function(context, redirect) {
	}],

	action: function(params, queryParams) {

		var metaInfo = {name: "Description", content: "Reservation list page for sauna"};
		DocHead.addMeta(metaInfo);

		ReactLayout.render(MainLayout, {
			content:
				<div>
					<h2 className="sub-header">Sauna</h2>
					<br/>
					<WeekList table={"sauna"}/>
				</div>
		});
	}
});

FlowRouter.route('/sofa', {
	name: 'sofa',
	triggersEnter: [function(context, redirect) {
	}],

	action: function(params, queryParams) {

		var metaInfo = {name: "Description", content: "Reservation list page for sofa"};
		DocHead.addMeta(metaInfo);

		ReactLayout.render(MainLayout, {
			content:
				<div>
					<h2 className="sub-header">Sofa</h2>
					<br/>
					<WeekList table={"sofa"}/>
				</div>
		});
	}
});

FlowRouter.route('/television', {
	name: 'television',
	triggersEnter: [function(context, redirect) {
	}],

	action: function(params, queryParams) {

		var metaInfo = {name: "Description", content: "Reservation list page for television"};
		DocHead.addMeta(metaInfo);

		ReactLayout.render(MainLayout, {
			content:
				<div>
					<h2 className="sub-header">Television</h2>
					<br/>
					<WeekList table={"television"}/>
				</div>
		});
	}
});

// Refresh when logging event occurs
if (Meteor.isClient) {
	Tracker.autorun(function() {
		// Reactive variable that causes reload
		var userId = Meteor.userId();
		if (FlowRouter.getRouteName()) {
			FlowRouter.reload();
		}
	});
}
