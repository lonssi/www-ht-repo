MainLayout = React.createClass({

	mixins: [ReactMeteorData],

	getMeteorData: function() {
		return {

		};
	},

	render() {

		return (
			<div className="main-layout">

				<NavBar/>

				<div className="container-fluid">
					<div className="row">
						<div className="col-md-12 main">
							{this.props.content}
						</div>
					</div>
				</div>

			</div>
			);
	}
});
