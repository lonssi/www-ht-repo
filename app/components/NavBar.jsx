const LoginButtons = BlazeToReact('loginButtons');

NavBar = React.createClass({

	mixins: [ReactMeteorData],

	getMeteorData: function() {
		return {

		};
	},

	collapseMenu: function() {
	    $(this.refs.target).collapse('hide');
	},

	render() {

		return (

			<div>
				<nav className="navbar navbar-inverse navbar-fixed-top">
					<div className="container-fluid">
						<div className="navbar-header">
							<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
								<span className="sr-only">Toggle navigation</span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
							</button>
							<a className="navbar-brand" href="/"><img src="/images/rlogo.svg" className="app-Logo"/></a>
						</div>
						<div id="navbar" className="navbar-collapse collapse" ref="target">
							<ul className="nav navbar-nav navbar-right">
								<li><a href="/reservations" onClick={this.collapseMenu}>My reservations</a></li>
								<li><a href="/laundry" onClick={this.collapseMenu}>Laundry</a></li>
								<li><a href="/sauna" onClick={this.collapseMenu}>Sauna</a></li>
								<li><a href="/sofa" onClick={this.collapseMenu}>Sofa</a></li>
								<li><a href="/television" onClick={this.collapseMenu}>Television</a></li>
								<li><a><LoginButtons/></a></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>

			);
	}

});
