ReservationHistoryItem = React.createClass({

	mixins: [ReactMeteorData],

	getMeteorData: function() {

		return {
		};
	},

	render() {

		// Time formatting
		var ts = this.props.time.toString();
		ts = moment(ts);
		date = ts.format("Do MMMM YYYY");
		day = ts.format("dddd")
		hour = ts.format("H");

		return (

			<tr className="myReservation">
				<td>
					{day} {date}
				</td>
				<td>
					{hour}:00
				</td>
				<td className="placeColumn">
					{this.props.table}
				</td>
			</tr>

			);
	}

});

ReservationHistoryList = React.createClass({

	mixins: [ReactMeteorData],

	getMeteorData: function() {

		var data = {reservations: []};
		var sub = Meteor.subscribe('myReservations');

		// Reservation data from the store
		data.reservationscount = ReservationStore.getReservationsCount();
		data.reservations = ReservationStore.findOldReservations();

		return data;

	},

	render() {

		// Map the reservation items
		var items = this.data.reservations.map(function(item) {
			return <ReservationHistoryItem {...item}
						key={item._id} />
		});

		return (
			<div>
				<h2 className="sub-header">Reservation history</h2>
				<br/>
				<div className="table-responsive">
					<table className="table table-striped">
						<thead>
							<tr>
								<th>Date</th>
								<th>Time</th>
								<th>Place</th>
							</tr>
						</thead>
						<tbody className="timeSlots">
							{items}
						</tbody>
					</table>
				</div>
			</div>
			);
	}
});
