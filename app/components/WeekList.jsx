WeekList = React.createClass({

	mixins: [ReactMeteorData],

	getMeteorData: function() {
		Meteor.subscribe('anonReservations');
		return {};
	},

	render() {

		var daysForward = 8;
		var startHour = 0;
		var endHour = 24;
		var now = moment();
		var table = this.props.table;

		now.milliseconds(0);
		now.seconds(0);
		now.minutes(0);

		// Generate a day list for each day (week forward)
		var componentList = _.map(_.range(daysForward), function(i) {

			var time = now.clone().add(i, 'day');

			// Current day is a specific case
			var start = (i == 0) ? time.hours() + 1 : startHour;
			start = (start < startHour) ? startHour : start;

			// Generate an unique key
			var key = 'day-item-' + time.unix();

			return <DayList startHour={start} endHour={endHour} ts={time} key={key} table={table}/>;

		});

		return (
				<div>{componentList}</div>
			);
	}
});
