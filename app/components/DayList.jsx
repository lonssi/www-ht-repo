HourItem = React.createClass({

	mixins: [ReactMeteorData],

	getMeteorData: function() {
		return {
			// Check if the reservation slot is available
			isAvailable: !ReservationStore.findReservationByTime({
				"time": this.props.ts,
				"table": this.props.table
			})
		};
	},

	getAvailableField : function() {

		// Check if the reservation slot is available
		// For styling and content
		if (this.data.isAvailable) {
			return "Free";
		} else {
			return "Reserved";
		}

	},

	getLoggedStatus : function() {

		// For css-styling
		if (Meteor.loggingIn() || Meteor.userId()) {
			return "logged-in";
		} else {
			return "not-logged-in"
		}

	},

	addReservation : function() {

		Dispatcher.dispatch({
			actionType: "ADD_RESERVATION_BUTTON_CLICKED",
			data: {
				time: this.props.ts,
				table: this.props.table
			}
		});

	},

	render() {

		return (

			<tr className={"tr-" + this.getAvailableField() + " tr-" + this.getLoggedStatus()}>
				<td>
					{this.props.currentHour}:00
				</td>
				<td>
					{this.getAvailableField()}
				</td>
				<td>
					<button onClick={this.addReservation} className="btn btn-sm btn-primary">Reserve</button>
				</td>
			</tr>

			);
	}

});

DayList = React.createClass({

	mixins: [ReactMeteorData],

	getMeteorData: function() {
		return {
			reservations: ReservationStore.getReservations().fetch()
		};
	},

	render() {

		// Variables from WeekList.jsx
		var startHour = this.props.startHour;
		var endHour = this.props.endHour;
		var ts = this.props.ts;
		var table = this.props.table;

		// Generate hour items for a day
		var items = _.map(_.range(startHour, endHour), function(i) {

			var t = ts.clone().hours(i);
			var key = 'hour-item-' + t.unix();
			return <HourItem currentHour={i} ts={t} key={key} table={table}/>;

		});

		// Time formatting
		date = ts.format("Do MMMM YYYY");
		day = ts.format("dddd");

		return (
			<div>
				<h5 className="sub-header">{day} {date}</h5>
				<div className="table-responsive">
					<table className="table table-striped">
						<thead>
							<tr>
								<th>Time</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody className="timeSlots">
							{items}
						</tbody>
					</table>
				</div>
			</div>
			);
	}
});
