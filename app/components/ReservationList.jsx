ReservationItem = React.createClass({

	mixins: [ReactMeteorData],

	getMeteorData: function() {
		return {
		};
	},

	removeReservation: function() {
		Dispatcher.dispatch({
			actionType: "REMOVE_RESERVATION_BUTTON_CLICKED",
			data: this.props._id
		});
	},

	render() {

		// Time formatting
		var ts = this.props.time.toString();
		ts = moment(ts);
		date = ts.format("Do MMMM YYYY");
		day = ts.format("dddd");
		hour = ts.format("H");

		return (

			<tr className="myReservation">
				<td>
					{day} {date}
				</td>
				<td>
					{hour}:00
				</td>
				<td className="placeColumn">
					{this.props.table}
				</td>
				<td>
					<button onClick={this.removeReservation}
						className="btn btn-sm btn-warning">Remove</button>
				</td>
			</tr>

			);
	}

});

ReservationList = React.createClass({

	mixins: [ReactMeteorData],

	getMeteorData: function() {

		var data = {reservations: []};
		var sub = Meteor.subscribe('myReservations');

		if (sub.ready()) {
			// Reservation data from the store
			data.reservationscount = ReservationStore.getReservationsCount();
			data.reservations = ReservationStore.findReservations({userId: Meteor.userId()});
		}

		return data;

	},

	render() {

		// Map the reservation items
		var items = this.data.reservations.map(function(item) {
			return <ReservationItem {...item}
						key={item._id} />
		});

		return (
			<div>
				<h2 className="sub-header">My reservations</h2>
				<br/>
				<div className="table-responsive">
					<table className="table table-striped">
						<thead>
							<tr>
								<th>Date</th>
								<th>Time</th>
								<th>Place</th>
								<th></th>
							</tr>
						</thead>
						<tbody className="timeSlots">
							{items}
						</tbody>
					</table>
				</div>
			</div>
			);
	}
});
