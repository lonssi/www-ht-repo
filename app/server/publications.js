// Publication of reservations for a single user
// Sorted by time
Meteor.publish('myReservations', function(options) {
	return Reservations.find({
		"userId": this.userId
	});
});

// Anonymous reservation publication, user data left out
Meteor.publish('anonReservations', function(options) {
	return Reservations.find({
		"time": {"$gte": new Date()}
	}, {
		fields: {userId: 0}
	});
});
