Reservations.allow({

	insert : function(userId, doc) {
		//Check if user is logged in, time is in right
		//format and the reservation doesn't already exist
		var notOccupied = !Reservations.findOne({"time": doc.time, "table": doc.table})
		return userId && doc.time instanceof Date && notOccupied;
	},

	update: function(userId, doc) {
		return false;
	},

	remove: function(userId, doc) {

		// Check if the reservation is in history
		var notHistory = (doc.time > new Date());

		// Return true if validated user
		return userId && doc.userId === userId && notHistory;
	}

});
