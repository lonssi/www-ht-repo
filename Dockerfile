# Base image: https://hub.docker.com/_/debian/
FROM debian:sid

MAINTAINER lauri.rapo@vionice.fi

ENV DEBIAN_FRONTEND noninteractive

# Update apt sources
RUN apt-get update && apt-get upgrade -y && apt-get install -y locales

# Configure timezone
RUN echo "UTC" > /etc/timezone && \
    dpkg-reconfigure -f noninteractive tzdata

# Configure locales
RUN sed -i 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
RUN /usr/sbin/locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Install required packages 
RUN apt-get update && apt-get install -y \
    python-pkg-resources \
    supervisor \
    wget \
    curl

# Install Node.js (latest version has problems with node-fibers, Meteor supports officially only 0.10.X)
RUN \
    cd /tmp && \
    wget https://nodejs.org/dist/latest-v0.10.x/node-v0.10.41-linux-x64.tar.gz && \
    tar xvzf node-*.tar.gz && \
    rm -f node-*.tar.gz && \
    mv ./node-v0.10*-linux-x64/ /opt/ && \
    ln -s /opt/node-v0.10*-linux-x64/bin/node /usr/bin/node && \
    ln -s /opt/node-v0.10*-linux-x64/bin/npm  /usr/bin/npm && \
    echo '\nexport PATH="node_modules/.bin:$PATH"' >> /root/.bashrc

# Install Meteor
RUN curl https://install.meteor.com/ | sh 

# Move source files into the container
COPY supervisord.conf /etc/supervisord.conf
COPY app/ /app/

# Build the app
RUN cd /app && meteor build --directory /build --architecture os.linux.x86_64
RUN cd /build/bundle/programs/server && npm install

# Exposed port 5000 will be automatically configured with dokku's vhost/nginx
ENV PORT 5000
EXPOSE 5000
ENV NODE_ENV production

# Persistent /log volume can be mounted
# Supervisord ensures that app keeps running, for details: see supervisord.conf
CMD mkdir -p /log/www-ht && /usr/bin/supervisord
